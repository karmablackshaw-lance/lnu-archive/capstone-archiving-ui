import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const lazyLoad = name => () => import(/* webpackChunkName: "[request]" */ `@/views/${name}.vue`)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: lazyLoad('Home')
  },
  {
    path: '/login',
    name: 'Login',
    component: lazyLoad('auth/Login')
  },
  {
    path: '/register',
    name: 'Register',
    component: lazyLoad('auth/Register')
  },
  {
    path: '/advanced/search',
    name: 'advanced-search',
    component: lazyLoad('archive/AdvancedSearchArchive')
  },
  {
    path: '/public-archive',
    name: 'public-archives',
    component: lazyLoad('archive/PublicArchive')
  },

  {
    path: '/dashboard',
    name: 'Dashboard',
    component: lazyLoad('dashboard/Dashboard'),
    meta: { auth: true }
  },
  {
    path: '/archive',
    name: 'Archive',
    component: lazyLoad('archive/Archive'),
    meta: { auth: true }
  },
  // {
  //   path: '/archive/create',
  //   name: 'ArchiveCreate',
  //   component: lazyLoad('archive/CreateArchive'),
  //   meta: { auth: true }
  // },
  {
    path: '/archive/:id',
    name: 'ArchiveDetail',
    component: lazyLoad('archive-details/ArchiveDetails'),
    meta: { auth: true }
  },
  {
    path: '/public/archive/:id',
    name: 'PublicArchiveDetail',
    component: lazyLoad('archive-details/ArchiveDetails')
  },
  {
    path: '/authors',
    name: 'Authors',
    component: lazyLoad('authors/Authors'),
    meta: { auth: true }
  },
  {
    path: '/students/:status',
    name: 'Students',
    component: lazyLoad('students/Students'),
    meta: { auth: true }
  },
  {
    path: '/tags',
    name: 'Tags',
    component: lazyLoad('tags/Tags'),
    meta: { auth: true }
  },
  {
    path: '/subjects',
    name: 'Subjects',
    component: lazyLoad('subjects/Subjects'),
    meta: { auth: true }
  },
  {
    path: '/faqs',
    name: 'Faqs',
    component: lazyLoad('faqs/Faqs'),
    meta: { auth: true }
  },
  {
    path: '/settings',
    name: 'Settings',
    component: lazyLoad('settings/Settings'),
    meta: { auth: true }
  },
  {
    path: '/backup',
    name: 'Backup',
    component: lazyLoad('backup/Backup'),
    meta: { auth: true }
  },
  {
    path: '*',
    name: 'page-not-found',
    component: lazyLoad('auth/Login')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  const token = window.localStorage.token

  if (!token && to.meta.auth) {
    return next({ name: 'Login' })
  }

  if (token && !to.meta.auth) {
    return next({ name: 'Dashboard' })
  }

  next()
})

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location) {
  return originalPush.call(this, location).catch(err => err)
}

export default router
