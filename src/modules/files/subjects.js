import axios from '@/config/axios'

export default {
  namespaced: true,

  state: {
    subjects: [],
    subjectsDashboard: [],
    subSubjects: [],
    homeSubjects: [],
    publicArchiveSubjects: []
  },

  mutations: {
    SET_STATE (state, data) {
      state[data.state] = data.value
    }
  },

  actions: {
    async getSubjectsDashboard ({ commit }, params) {
      try {
        const response = await axios.get('/subjects/dashboard', { params })
        const list = response.data

        commit('SET_STATE', { state: 'subjectsDashboard', value: list })
        return list
      } catch (error) {
        throw new Error(error)
      }
    },

    async getSubSubjectsList ({ commit }, params) {
      try {
        const response = await axios.get('/subjects/sub', { params })
        const list = response.data

        commit('SET_STATE', { state: 'subSubjects', value: list })
        return list
      } catch (error) {
        throw new Error(error)
      }
    },

    async getHomeSubjectsList ({ commit }, params) {
      try {
        const response = await axios.get('/subjects/home', { params })
        const list = response.data

        commit('SET_STATE', { state: 'homeSubjects', value: list })
        return list
      } catch (error) {
        throw new Error(error)
      }
    },

    async getPublicArchiveSubjectsList ({ commit }, params) {
      try {
        const response = await axios.get('/subjects/public-archive', { params })
        const list = response.data

        commit('SET_STATE', { state: 'publicArchiveSubjects', value: list })
        return list
      } catch (error) {
        throw new Error(error)
      }
    },

    async getSubjectsList ({ commit }, params) {
      try {
        const response = await axios.get('/subjects', { params })
        const list = response.data

        commit('SET_STATE', { state: 'subjects', value: list })
        return list
      } catch (error) {
        throw new Error(error)
      }
    },

    createSubject ({ commit }, payload) {
      return axios.post('/subjects', payload)
    },

    createSubSubject ({ commit }, payload) {
      return axios.post('/subjects/sub', payload)
    },

    modifySubject ({ commit }, payload) {
      return axios.put('/subjects', payload)
    },

    modifySubSubject ({ commit }, payload) {
      return axios.put('/subjects/sub', payload)
    },

    deleteSubject ({ commit }, data) {
      return axios.delete('/subjects', { data })
    },

    deleteSubSubject ({ commit }, data) {
      return axios.delete('/subjects/sub', { data })
    },

    restoreSubject ({ commit }, payload) {
      return axios.put('/subjects/restore', payload)
    },

    restoreSubSubject ({ commit }, payload) {
      return axios.put('/subjects/restore/sub', payload)
    }
  }
}
