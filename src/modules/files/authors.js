import axios from '@/config/axios'

export default {
  namespaced: true,

  state: {
    authors: [],
    authorsDashboard: [],
    publishedAuthors: [],
    publicArchiveAuthors: []
  },

  mutations: {
    SET_STATE (state, data) {
      state[data.state] = data.value
    }
  },

  actions: {
    async getAuthorsDashboard ({ commit }, params) {
      try {
        const response = await axios.get('/authors/dashboard', { params })
        const list = response.data

        commit('SET_STATE', { state: 'authorsDashboard', value: list })
        return list
      } catch (error) {
        throw new Error(error)
      }
    },

    async getAuthorsList ({ commit }, params) {
      try {
        const response = await axios.get('/authors', { params })
        const list = response.data

        commit('SET_STATE', { state: 'authors', value: list })
        return list
      } catch (error) {
        throw new Error(error)
      }
    },

    async getPublicArchiveAuthorsList ({ commit }, params) {
      try {
        const response = await axios.get('/authors/public-archive', { params })
        const list = response.data

        commit('SET_STATE', { state: 'publicArchiveAuthors', value: list })
        return list
      } catch (error) {
        throw new Error(error)
      }
    },

    createAuthor ({ commit }, payload) {
      return axios.post('/authors', payload)
    },

    modifyAuthor ({ commit }, payload) {
      return axios.put('/authors', payload)
    },

    async getPublishedAuthorsList ({ commit }, params) {
      try {
        const response = await axios.get('/authors/publishers', { params })
        const list = response.data

        commit('SET_STATE', { state: 'publishedAuthors', value: list })
        return list
      } catch (error) {
        throw new Error(error)
      }
    },

    deleteAuthor ({ commit }, data) {
      return axios.delete('/authors', { data })
    },

    restoreAuthor ({ commit }, payload) {
      return axios.put('/authors/restore', payload)
    }
  }
}
