import axios from '@/config/axios'

export default {
  namespaced: true,

  state: {
    list: [],
    count: 0,
    authUser: null,
    usersDashboard: {}
  },

  mutations: {
    SET_STATE (state, data) {
      state[data.state] = data.value
    }
  },

  actions: {
    async getAuthUser ({ commit }) {
      try {
        const response = await axios.get('/users/authenticated')

        commit('SET_STATE', { state: 'authUser', value: response.data })

        return response.data
      } catch (err) {
        const error = new Error(err)
        error.response = err.response
        throw error
      }
    },

    updateAuthenticatedAccount ({ commit }, payload) {
      return axios.patch('/users/authenticated', payload)
    },

    update ({ commit }, payload) {
      return axios.patch('/users', payload)
    },

    register ({ commit }, payload) {
      return axios.post('/users', payload)
    },

    async getUsersDashboard ({ commit }, params) {
      try {
        const response = await axios.get('/users/dashboard', { params })
        const list = response.data

        commit('SET_STATE', { state: 'usersDashboard', value: list })
        return list
      } catch (error) {
        throw new Error(error)
      }
    },

    async list ({ commit }, params) {
      try {
        const response = await axios.get('/users', { params })

        commit('SET_STATE', { state: 'list', value: response.data.list })
        commit('SET_STATE', { state: 'count', value: response.data.count })

        return response.data
      } catch (error) {
        throw error.response
      }
    }
  }
}
