import axios from '@/config/axios'

export default {
  namespaced: true,

  state: {
    archives: [],
    recentArchivesList: [],
    publicArchivePublishDates: [],
    archivesDashboard: [],
    foundData: null
  },

  mutations: {
    SET_STATE (state, data) {
      state[data.state] = data.value
    }
  },

  actions: {
    async getArchivesDashboard ({ commit }, params) {
      try {
        const response = await axios.get('/archives/dashboard', { params })
        const list = response.data

        commit('SET_STATE', { state: 'archivesDashboard', value: list })
        return list
      } catch (error) {
        throw new Error(error)
      }
    },

    async getArchivesList ({ commit }, params) {
      try {
        const response = await axios.get('/archives', { params })
        const list = response.data

        commit('SET_STATE', { state: 'archives', value: list })
        return list
      } catch (error) {
        throw new Error(error)
      }
    },

    async getPublicArchivePublishDatesList ({ commit }, params) {
      try {
        const response = await axios.get('/archives/public-archive/publish-dates', { params })
        const list = response.data

        commit('SET_STATE', { state: 'publicArchivePublishDates', value: list })
        return list
      } catch (error) {
        throw new Error(error)
      }
    },

    async getRecentList ({ commit }, params) {
      try {
        const response = await axios.get('/archives/recent', { params })
        const list = response.data

        commit('SET_STATE', { state: 'recentArchivesList', value: list })
        return list
      } catch (error) {
        throw new Error(error)
      }
    },

    async find ({ commit }, id) {
      try {
        const response = await axios.get(`/archives/${id}`)

        commit('SET_STATE', { state: 'foundData', value: response.data })

        return response.data
      } catch (error) {
        throw new Error(error)
      }
    },

    postArchive ({ commit }, payload) {
      return axios.post('/archives', payload)
    },

    restoreArchive ({ commit }, payload) {
      return axios.post('/archives/restore', payload)
    },

    deleteArchive ({ commit }, data) {
      return axios.delete('/archives', { data })
    },

    updateArchive ({ commit }, payload) {
      return axios.put('/archives', payload)
    }
  }
}
