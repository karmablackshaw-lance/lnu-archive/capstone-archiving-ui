import axios from '@/config/axios'

export default {
  namespaced: true,

  state: {
    backupList: []
  },

  mutations: {
    SET_STATE (state, data) {
      state[data.state] = data.value
    }
  },

  actions: {
    async getBackupList ({ commit }, payload) {
      try {
        const response = await axios.get('/backup', payload)
        commit('SET_STATE', { state: 'backupList', value: response.data })
        return response.data
      } catch (err) {
        const error = new Error()
        error.response = err.response
        throw error
      }
    },

    backupDatabase ({ commit }, payload) {
      return axios.post('/backup', payload)
    },

    deleteBackup ({ commit }, data) {
      return axios.delete('/backup', { data })
    },

    restore ({ commit }, payload) {
      return axios.post(`/backup/restore/${payload.backupID}`)
    }
  }
}
