import axios from '@/config/axios'

export default {
  namespaced: true,

  state: {
    list: [],
    count: 0
  },

  mutations: {
    SET_STATE (state, payload) {
      state[payload.state] = payload.value
    }
  },

  actions: {
    async list ({ commit }, params) {
      try {
        const response = await axios.get('/tags', { params })

        commit('SET_STATE', { state: 'list', value: response.data.list })
        commit('SET_STATE', { state: 'count', value: response.data.count })

        return response.data
      } catch (error) {
        console.error(error)
        throw error
      }
    },

    register ({ commit }, payload) {
      return axios.post('/tags', payload)
    }
  }
}
