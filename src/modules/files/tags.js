import axios from '@/config/axios'

export default {
  namespaced: true,

  state: {
    tags: [],
    tagsDashboard: [],
    popularTagsList: [],
    publicArchiveTags: []
  },

  mutations: {
    SET_STATE (state, data) {
      state[data.state] = data.value
    }
  },

  actions: {
    async getTagsDashboard ({ commit }, params) {
      try {
        const response = await axios.get('/tags/dashboard', { params })
        const list = response.data

        commit('SET_STATE', { state: 'tagsDashboard', value: list })
        return list
      } catch (error) {
        throw new Error(error)
      }
    },

    async getTagsList ({ commit }, params) {
      try {
        const response = await axios.get('/tags', { params })
        const list = response.data

        commit('SET_STATE', { state: 'tags', value: list })
        return list
      } catch (error) {
        throw new Error(error)
      }
    },

    async getPublicArchiveTagsList ({ commit }, params) {
      try {
        const response = await axios.get('/tags/public-archive', { params })
        const list = response.data

        commit('SET_STATE', { state: 'publicArchiveTags', value: list })
        return list
      } catch (error) {
        throw new Error(error)
      }
    },

    async getPopularTagsList ({ commit }, params) {
      try {
        const response = await axios.get('/tags/popular', { params })
        const list = response.data

        commit('SET_STATE', { state: 'popularTagsList', value: list })
        return list
      } catch (error) {
        throw new Error(error)
      }
    },

    createTag ({ commit }, payload) {
      return axios.post('/tags', payload)
    },

    modifyTag ({ commit }, payload) {
      return axios.put('/tags', payload)
    },

    deleteTag ({ commit }, data) {
      return axios.delete('/tags', { data })
    },

    restoreTag ({ commit }, payload) {
      return axios.put('/tags/restore', payload)
    }
  }
}
