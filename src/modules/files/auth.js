import axios from '@/config/axios'
import jwt from 'jsonwebtoken'

export default {
  namespaced: true,

  state: {
    token: window.localStorage.token || null
  },

  mutations: {
    SET_STATE (state, data) {
      state[data.state] = data.value
    }
  },

  getters: {
    tokenDecoded: state => {
      const token = state.token
      const decoded = jwt.decode(token)
      return decoded && decoded.data
    }
  },

  actions: {
    async getLoginToken ({ commit }, payload) {
      try {
        const response = await axios.post('/auth', payload)
        const token = response.data

        axios.defaults.headers.common.Authorization = `Bearer ${token}`
        commit('SET_STATE', { state: 'token', value: token })
        return token
      } catch (err) {
        window.localStorage.removeItem('token')
        const error = new Error()
        error.response = err.response
        throw error
      }
    },

    async revalidate ({ commit }) {
      try {
        const response = await axios.post('/auth/revalidate')

        commit('SET_STATE', { state: 'token', value: response.data })

        return response.data
      } catch (error) {
        throw error.response
      }
    },

    clearAuthUser ({ commit }, payload) {
      commit('SET_STATE', { state: 'token', value: '' })
      commit('SET_STATE', { state: 'token', value: '' })
    }
  }
}
