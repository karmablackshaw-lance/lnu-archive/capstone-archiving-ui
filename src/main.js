import '@mdi/font/css/materialdesignicons.min.css'

import './assets/scss/global.scss'
import './assets/scss/_hover.scss'

import 'sweetalert2/dist/sweetalert2.min.css'

import App from './App.vue'
import router from './router'
import store from './modules'

// configs
import Vue from './config/Vue'
import vuetify from './config/vuetify'
import './config/axios'

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
