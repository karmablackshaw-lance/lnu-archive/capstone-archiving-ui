/* eslint-disable require-await */
// eslint-disable no-undef
import axios from 'axios'
import router from '../router'
const _get = require('lodash/get')

axios.defaults.baseURL = process.env.VUE_APP_API_BASE_URL
axios.defaults.headers.common.Authorization = `Bearer ${window.localStorage.token}`

axios.interceptors.response.use(
  async response => response,
  async err => {
    if (_get(err, 'response.data') === 'jwt malformed') {
      window.localStorage.clear()
      router.push({ name: 'login' })
    }

    if (_get(err, 'response.status') === 401) {
      window.localStorage.clear()
      router.push({ name: 'login' })
    }

    return Promise.reject(err)
  }
)

export default axios
