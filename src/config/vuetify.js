import '@mdi/font/css/materialdesignicons.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css' // Ensure you are using css-loader
import Vue from 'vue'
import Vuetify from 'vuetify/lib'

Vue.use(Vuetify)

const theme = {
  primary: '#4CAF50',
  secondary: '#9C27b0',
  accent: '#9C27b0',
  info: '#00CAE3'
}

export default new Vuetify({
  theme: {
    dark: Number(window.localStorage.getItem('isDark')),
    options: {
      themeCache: {
        get: key => window.localStorage.getItem(key),
        set: (key, value) => window.localStorage.setItem(key, value)
      }
    },
    themes: {
      dark: theme,
      light: theme
    }
  },
  icons: {
    iconfont: 'md',
    values: {}
  }
})
