import Vue from 'vue'
import VueMasonry from 'vue-masonry-css'
import VueLocalStorage from 'vue-localstorage'

import _upperFirst from 'lodash/upperFirst'
import _camelCase from 'lodash/camelCase'

// Plugins
import Swal from '@/assets/plugins/swal'

// Mixins
import { statusWrapper } from '@/assets/mixins/StatusWrapper'
import globals from '@/assets/mixins/global'
import variables from '@/assets/mixins/variables'
import snackbar from '@/assets/mixins/alert'

// Base Components
const requireComponent = require.context('../components/', false, (/Base[A-Z]\w+\.(vue|js)$/))
requireComponent.keys().forEach(fileName => {
  // Get component config
  const componentConfig = requireComponent(fileName)

  // Get PascalCase name of component
  const componentName = _upperFirst(_camelCase(fileName.split('/').pop().replace(/\.\w+$/, '')))

  Vue.component(componentName, componentConfig.default || componentConfig)
})

Vue.config.productionTip = false

Vue.mixin(statusWrapper)
Vue.mixin(globals)
Vue.mixin(variables)
Vue.mixin(snackbar)

Vue.use(Swal)
Vue.use(VueLocalStorage)

Vue.config.errorHandler = function (err, vm, info) {
  console.error('Vue error: ', err)
}

Vue.use(VueMasonry)

export default Vue
