/**
 * Description:
 *
 * Automatically set the loading and empty flags by
 * wrapping your functions inside a higher order function
 *
 * Usage:
 * import { statusWrapper } from '@/assets/js/mixins/base/StatusWrapper'
 *
 * mixins: [
 *  statusWrapper
 * ],
 *
 * methods: {
 *   someMethod () {
 *     const config = {
 *       data: 'goalServeOdds',
 *       loading: 'isDataLoading'
 *     }
 *
 *      await this.statusWrapper(async () => {
 *       await this.fetchHelloWorld()
 *     }, config)
 *   }
 * }
 */

import moment from 'moment'

import _get from 'lodash/get'
import _pickBy from 'lodash/pickBy'
import _last from 'lodash/last'

export default {
  filters: {
    $truncateString (str, len) {
      return str.length > len
        ? str.slice(0, len) + '...'
        : str
    },

    $humanDate: date => {
      return date ? moment(date).format('MMMM DD, YYYY') : ''
    },

    $adminDate: date => {
      return date ? moment(date).format('dddd, MMMM Do YYYY, HH:mm:ss') : ''
    },

    $toUpper: str => {
      return String(str || '').toUpperCase()
    },

    $toDecimal: number => new Intl.NumberFormat('en-US', { style: 'decimal' }).format(number)
  },

  data () {
    return {
      isPageLoading: true
    }
  },

  computed: {
    $env () {
      const env = process.env
      const newEnv = {}

      for (const key in env) {
        const newKey = key.replace('VUE_APP_', '').toLowerCase()
        newEnv[newKey] = env[key]
      }

      return newEnv
    }
  },

  methods: {
    $trigger (e = {}) {
      const currQuery = _pickBy(e, Boolean)

      if (e.clear) {
        for (const key in this.params) {
          this.params[key] = ''
        }
      }

      const query = _pickBy({ ...currQuery }, Boolean)
      this.$router.push({ query })
    },

    $sleep: time => new Promise(resolve => setTimeout(resolve, time)),

    $handleError (error) {
      const message = _get(error, 'response.data.message')
      this.$alert({ type: 'error', text: message })
    },

    $generateNumber (index, count) {
      const params = this.$route.query
      const page = params.page || 1
      const rows = params.rows || 15
      const sort = params.sort

      return sort === 'desc'
        ? count - page * rows + rows - index
        : count - (count - index - 1) + rows * (page - 1)
    },

    $pad (n, width, z) {
      z = z || '0'
      n = n + ''
      return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n
    },

    $formatNumber (num) {
      return new Intl.NumberFormat('en-US', { style: 'decimal' }).format(num)
    },

    $toCDN (str) {
      return str[0] === '/'
        ? `${process.env.VUE_APP_API_BASE_URL}${str}`
        : `${process.env.VUE_APP_API_BASE_URL}/${str}`
    },

    $download (uri) {
      fetch(uri)
        .then(response => response.blob())
        .then(blob => {
          const url = URL.createObjectURL(blob)

          const a = document.createElement('a')

          a.href = url
          a.download = _last(uri.split('/')) || 'download'

          const clickHandler = () => {
            setTimeout(() => {
              URL.revokeObjectURL(url)
              this.removeEventListener('click', clickHandler)
            }, 150)
          }

          a.addEventListener('click', clickHandler, false)

          a.click()

          return blob
        })
    }
  }
}
