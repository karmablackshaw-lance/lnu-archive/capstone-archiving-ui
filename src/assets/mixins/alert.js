/**
 * Description:
 *
 * Automatically set the loading and empty flags by
 * wrapping your functions inside a higher order function
 *
 * Usage:
 * import { statusWrapper } from '@/assets/js/mixins/base/StatusWrapper'
 *
 * mixins: [
 *  statusWrapper
 * ],
 *
 * methods: {
 *   someMethod () {
 *     const config = {
 *       data: 'goalServeOdds',
 *       loading: 'isDataLoading'
 *     }
 *
 *      await this.statusWrapper(async () => {
 *       await this.fetchHelloWorld()
 *     }, config)
 *   }
 * }
 */

import Swal from 'sweetalert2'
// import store from '@/modules'
const getKey = (key, obj) => obj[key] === undefined ? obj.default : obj[key]

export default {
  methods: {
    $setAlert ({ text, type }) {
      const defaultTitle = getKey(type, {
        success: 'Success',
        warning: 'Warning',
        error: 'Oops!'
      })

      const defaultText = getKey(type, {
        success: 'Your request has been processed successfully',
        warning: 'Invalid request, please contact administration',
        error: 'Something went wrong. Please try again!'
      })

      return Swal.fire({
        title: defaultTitle,
        text: text || defaultText,
        // icon: type,
        confirmButtonText: 'Cool'
      })
    }

    // $setAlert ({ text, type }) {
    //   const icon = getKey(type, {
    //     success: 'mdi-check-circle',
    //     warning: 'mdi-alert-circle',
    //     error: 'mdi-skull'
    //   })

    //   const defaultText = getKey(type, {
    //     success: 'Your request has been processed successfully',
    //     warning: 'Invalid request, please contact administration',
    //     error: 'Something went wrong. Please try again!'
    //   })

    //   store.commit('SET_ALERT', {
    //     color: type,
    //     message: text || defaultText,
    //     icon: icon
    //   })
    // }
  }
}
