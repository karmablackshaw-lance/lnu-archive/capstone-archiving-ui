/**
 * Description:
 *
 * Automatically set the loading and empty flags by
 * wrapping your functions inside a higher order function
 *
 * Usage:
 * import { statusWrapper } from '@/assets/js/mixins/base/StatusWrapper'
 *
 * mixins: [
 *  statusWrapper
 * ],
 *
 * methods: {
 *   someMethod () {
 *     const config = {
 *       data: 'goalServeOdds',
 *       loading: 'isDataLoading'
 *     }
 *
 *      await this.statusWrapper(async () => {
 *       await this.fetchHelloWorld()
 *     }, config)
 *   }
 * }
 */

import _camelCase from 'lodash/camelCase'

export default {
  computed: {
    $env () {
      const env = process.env
      const variables = {}
      for (const currVar in env) {
        const currVal = env[currVar]

        if (currVar.indexOf('VUE_APP') !== -1) {
          const name = _camelCase(currVar.replace('VUE_APP', ''))
          variables[name] = currVal
        }
      }
      return variables
    }
  }
}
