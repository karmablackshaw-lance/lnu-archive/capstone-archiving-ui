/**
 * Description:
 *
 * Automatically set the loading and empty flags by
 * wrapping your functions inside a higher order function
 *
 * Usage:
 * import { statusWrapper } from '@/assets/js/mixins/base/StatusWrapper'
 *
 * mixins: [
 *  statusWrapper
 * ],
 *
 * methods: {
 *   someMethod () {
 *     const config = {
 *       data: 'goalServeOdds',
 *       loading: 'isDataLoading'
 *     }
 *
 *      await this.statusWrapper(async () => {
 *       await this.fetchHelloWorld()
 *     }, config)
 *   }
 * }
 */

import _set from 'lodash/set'
import _get from 'lodash/get'
import _isEmpty from 'lodash/isEmpty'

export const statusWrapper = {
  methods: {

    async statusWrapper (fn, { data, empty = 'isEmpty', loading = 'isLoading' }) {
      _set(this, empty, false)
      _set(this, loading, true)
      await fn()
      _set(this, loading, false)
      const dataList = Array.isArray(data) ? data.every(x => _isEmpty(_get(this, x))) : _isEmpty(_get(this, data))
      _set(this, empty, dataList)
    }
  }
}
