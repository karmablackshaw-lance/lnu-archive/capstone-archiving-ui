import Swal from 'sweetalert2'

const getKey = (key, obj) => obj[key] === undefined ? obj.default : obj[key]

export default {
  install (Vue) {
    Vue.prototype.$swal = Swal

    Vue.prototype.$alert = function ({ title, text, type, confirmButtonText } = {}) {
      const defaultTitle = getKey(type, {
        success: 'Success',
        warning: 'Warning',
        error: 'Error!'
      })

      const defaultText = getKey(type, {
        success: 'Your request has been processed successfully',
        warning: 'Invalid request, please contact administration',
        error: 'Something went wrong. Please try again!'
      })

      return Swal.fire({
        title: title || defaultTitle,
        text: text || defaultText,
        // icon: type,
        confirmButtonText: confirmButtonText || 'Okay'
      })
    }

    Vue.prototype.$prompt = async function ({ title, text, confirmButtonText, success } = {}) {
      const defaultTitle = 'Are you sure?'
      const defaultText = "You won't be able to revert this!"

      const response = await Swal.fire({
        title: title || defaultTitle,
        text: text || defaultText,
        icon: 'warning',
        cancelButtonColor: '#d33',
        showCancelButton: true,
        confirmButtonText: confirmButtonText || 'Yes!'
      })

      if (response.isConfirmed) {
        success()
      }
    }
  }
}
